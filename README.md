fcrypto
=======

Introduction
------------
fcrypto is a small cryptography class to ease the use of python cryptography
libraries. It's main goal is to provide safe file encryption methods with a
simplified class interface.

Installation
------------

### Production Version
fcrypto is available on PyPI and has only been tested on Python 3. To install,
use the following:

```bash
$ pip install fcrypto
```

### Development Version
To get the latest development snapshot, you'll need to first clone the
repository:

```bash
$ git clone https://gitlab.urent.org/py/fcrypto.git
$ cd fcrypto
$ virtualenv ~/.virtualenvs/fcrypto
$ source ~/.virtualenvs/fcrypto/bin/activate
(fcrypto)$ pip install -r requirements.txt
(fcrypto)$ python setup.py install
```

Testing your installation
-------------------------

You can test your installation using the following:

```bash
$ python -c "import fcrypto; print(fcrypto.fcrypto.CURRENT_VERSION)"
1
```

You should get the fcrypto version number back.

Sample usage
------------
To encrypt a simple message:

```python
from fcrypto import fcrypto
crypto = fcrypto("a very long and complicated passphrase")
metadata = crypto.metadata()    # this metadata needs to be saved somewhere safe
block = "a secret message to encrypt"
encrypted_block = crypto.encrypt(block)
```

To decrypt a simple message:
```python
from fcrypto import fcrypto
crypto = fcrypto("a very long and complicated passphrase", metadata)
block = crypto.decrypt(encrypted_block)
```

To encrypt a file:

```python
from fcrypto import fcrypto
crypto = fcrypto("a very long and complicated passphrase")
metadata = crypto.metadata()    # this metadata needs to be saved somewhere safe
with open("/path/to/cleartext_file", "rb") as _if:
    with open("/path/to/encrypted_file", "wb") as _of:
        crypto.encrypt_file(_if, _of)
```

To decrypt a file:

```python
from fcrypto import fcrypto
crypto = fcrypto("a very long and complicated passphrase", metadata)
with open("/path/to/encrypted_file", "rb") as _if:
    with open("/path/to/cleartext_file", "wb") as _of:
        crypto.decrypt(_if, _of)
```

It is important to note that both the passphrase and the returned metadata must
be preserved for decrypting the file. The metadata contains the following
crucial, and secret, information:

* Encryption block size
* Block encryption mode
* Key-derivation function (KDF)
* The number of iteration for the KDF
* The salt used by the KDF
* Version of the library
* The cipher used for encryption


How it works
------------
When the class is first initialized, the passphrase will be stored internally.
The metadata parameter should be either:

 - None (or omitted), which means a set of default arguments will be used, in
   conjunction with a randomly generated salt. As noted above, this random salt
   as well as other algorithm information are crucial for decrypting data and
   must be kept in a safe place. 
 - A previously returned metadata.

The encryption process will:

 - Use the input block data or read it from input file.
 - For each block:
   - Generate a random IV and encrypt the IV/block data using a key
     derived from the initial passphrase and salt.
   - Generate an HMAC from the encrypted IV/block using another derived key.
   - The HMAC will then be prepended to the encrypted data to form the final
     encrypted block.
 - Prepend the fcrypto version number to the encrypted data blob.
