from distutils.core import setup

setup(
    name = "fcrypto",
    version = "1.0.1",
    py_modules = ['fcrypto'],
    description = "Simple encryption helper",
    author = "Laurent Duchesne",
    author_email = "l@urent.org",
    url = "https://bitbucket.org/lduchesne/fcrypto",
    classifiers = [
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Topic :: Security :: Cryptography",
        "Topic :: Software Development :: Libraries",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
)
